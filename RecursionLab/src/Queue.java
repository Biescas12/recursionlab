
public interface Queue<E> {
	
	void enqueue(E e); //inserts new element at the end
	
	E dequeue(); //removes and returns first inserted element. Return null if empty.
	
	E first(); //returns element at the end
	
	int size();
	
	boolean isEmpty();
}
