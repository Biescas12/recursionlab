import java.util.ArrayList;

public class SLL<E> {

	public static void main(String[] args) {
		SLL<Integer> list = new SLL<>();
	
		list.addFirst(2);
		list.addFirst(9);
		list.addFirst(3);
		list.addFirst(4);
		list.addFirst(1);
		list.addFirst(6);
		while(!list.isEmpty()) {
			System.out.println(list.removeFirst());
		}
	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		public Node() {
			element = null;
			next = null;
		}

		public Node(E element, Node<E> next) {
			this.element = element;
			this.next = next;
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
	}

	private Node<E> header = null;
	private int size = 0;

	public void addFirst(E e) {
		Node<E> nuevo = new Node<>(e, header);
		header = nuevo;
		size++;
	}
	
	public E removeFirst() {
		if(this.isEmpty()) {
			return null;
		}
		Node<E> temp = header;
		E etr = temp.getElement();
		header = temp.getNext();
		temp.setNext(null);
		size--;
		return etr;
	}
	
	public int size() {
		return this.size;
	}
	
	public boolean isEmpty() {
		return this.size==0;
	}

	public String toString() { 
		String s = ""; 
		Node<E> current = header; 
		while (current != null) { 
			s += " " + current.getElement(); 
			current = current.getNext(); 
		}
		return s; 
	}

	public void reverse() {
		if(size > 1) {
			header = recReverse(header).first();
		}
	}

	private Pair<Node<E>> recReverse(Node<E> first) {
		if(first == null) {
			return new Pair<>(first,null);
		}

		if(first.getNext()==null) {
			Pair<Node<E>> pair = new Pair<>(first, header);
			this.header = first;
			return pair;
		}
		recReverse(first.getNext());
		first.getNext().setNext(first);
		first.setNext(null);
		return new Pair<>(this.header, first);

	}

	public ArrayList<Pair<E>> consecutiveIncreasingPairs() { 
		ArrayList<Pair<E>> result = new ArrayList<>();     // an empty ArrayList object
		if (size > 0) 
			recCIP(header, result); 
		return result; 
	}

	private void recCIP(Node<E> first, ArrayList<Pair<E>> result) {
		if(!(first==null || first.getNext()==null)) {
			if(((Comparable<E>) first.getElement()).compareTo(first.getNext().getElement()) < 0) {
				result.add(new Pair<E>(first.getElement(), first.getNext().getElement()));
			}
			recCIP(first.getNext(), result);
		}
	}

}


