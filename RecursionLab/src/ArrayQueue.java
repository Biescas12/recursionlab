
public class ArrayQueue<E> implements Queue<E> {
	
	public static void main(String[] args) {
		ArrayQueue<Integer> q = new ArrayQueue<>();
		q.enqueue(1);
		q.enqueue(3);
		q.enqueue(7);
		q.enqueue(4);
		q.enqueue(9);
		q.enqueue(5);
		while(!q.isEmpty()) {
			System.out.println(q.dequeue());
		}
	}
	private SimpleList<E> list;
	
	public ArrayQueue() {
		list = new CircularArraySimpleList<>();
	}
	
	@Override
	public void enqueue(E e) {
		if(this.isEmpty()) {
			this.list.addFirst(e);
		}
		else {
			list.addLast(e);
		}
		
	}

	@Override
	public E dequeue() {
		return list.removeFirst();
	}

	@Override
	public E first() {
		return list.first();
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

}